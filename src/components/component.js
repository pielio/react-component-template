import '../../style/YourComponent.less';

import React from 'react';
import PLComp from 'plcomp';
import PropTypes from 'prop-types';


export default class YourComponent extends PLComp {

	constructor( props ) {

		super( props );
		this.bindMethods();
		this.setInitialState();
	}

	bindMethods() {

		//bind your methods here.
	}

	setInitialState() {

		//set your state here.
	}

	render() {

		return (
			<div className={ this.component_classes() } style={ this.component_style() }>
				<span>Start here</span>
			</div>
		);
	}

	component_classes( cls ) {

		var options,
			cls;

		options = {};
		cls = [ 'your-component' ];
		cls = super.component_classes( cls, options );
		return cls.join(' ');
	}

	component_style() {

		var style,
			options;

		style = {};
		options = {};

		style = super.component_style( style, options );
		return style;
	}

	content_classes() {

		var options,
			cls;

		options || {};
		cls = [ 'your-component-content' ];
		cls = super.content_classes( cls, options );
		return cls;
	}

	content_style() {

		var style,
			options;

		style = {};
		options = {};

		style = super.content_style( style, options );
		return style;
	}

}

YourComponent.propTypes = {

}

YourComponent.defaultProps = {

}
