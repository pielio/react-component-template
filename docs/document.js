#!/usr/bin/env node
var spawnSync,
	path,
	fs,
	src_dir,
	doc_target,
	md_target,
	docstring,
	docs,
	outstruct,
	keysplit,
	newkey,
	markdown,
	packageContents,
	repoGitURL,
	repoURL;

spawnSync = require('child_process').spawnSync;
path = require('path');
fs = require('fs');


/*functions*/
function stringOfLength( string, length ) {

	var newString = '';
	for( var i = 0; i < length; i++ ) {
		newString += string;
	}
	return newString;
}

function generateTitle( name ) {
	var title = '`' + name + '` (component)';
	return title + '\n';
}

function generateDescription( description ) {

	return description + '\n' + 'repo: [' + repoGitURL + '](' + repoURL + ')';
}

function generatePropType( type ) {

	var values;
	if( Array.isArray( type.value ) ) {
		values = '(' +
		type.value.map( function( typeValue ) {
			return typeValue.name || typeValue.value;
		}).join('|') +
		')';
	} else {
		values = type.value;
	}

	return 'type: `' + type.name + ( values ? values : '' ) + '`\n';
}

function generatePropDefaultValue( value ) {

	return 'defaultValue: `' + value.value + '`\n';
}

function generateProps( props ) {

	var title = 'Props';
	return (
		title + '\n' +
		stringOfLength( '-', title.length ) + '\n' + '\n' +
		Object.keys( props ).sort().map( function( propName ) {
			return generateProp( propName, props[ propName ] );
		}).join('\n')
	);
}

function generateProp( propName, prop ) {
	return(
		'### `' + propName + '`' + ( prop.required ? ' (required)' : '' ) + '\n' +
		'\n' +
		( prop.description ? prop.description + '\n\n' : '' ) +
		( prop.type ? generatePropType( prop.type ) : '' ) +
		( prop.defaultValue ? generatePropDefaultValue( prop.defaultValue ) : '' ) +
		'\n'
	);
}

function generateMarkdown( name, reactAPI ) {

	var markdownString =
		generateTitle( name ) + '\n' +
		generateDescription( reactAPI.description ) + '\n' +
		generateProps( reactAPI.props || {} );

	return markdownString;
}


src_dir = path.join( __dirname, '..', 'src', 'components' );
doc_target = path.join( __dirname, 'documentation' ) + '/docs.json';
md_target = path.join( __dirname, '..', 'readme.md' );

docstring = spawnSync( 'react-docgen', [ src_dir ] ).stdout.toString();
docs = JSON.parse( docstring );

//get the package version.
packageContents = JSON.parse( fs.readFileSync( path.join( __dirname, '..' ) + '/package.json', { encoding: 'utf8' } ) );
repoGitURL = packageContents.repository.url;
var a = repoGitURL.split('@')[ 1 ];
a = a.replace( /:/g, '/' );
a = a.replace( /.git/g, '' );
repoURL = 'https://' + a;

//get the repo uri.


outstruct = Object.keys( docs ).reduce( ( c, dockey ) => {
	keysplit = dockey.split('/');
	newkey = keysplit[ keysplit.length - 1 ];
	c.components[ newkey ] = docs[ dockey ];
	c.markdown += generateMarkdown( newkey, docs[ dockey ] );
	return c;
}, { components: {}, markdown: '' } );

fs.writeFileSync( doc_target, JSON.stringify( outstruct, null, 2 ) );
fs.writeFileSync( md_target, outstruct.markdown );
