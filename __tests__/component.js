import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Component from '../src/components/component';

test( 'TESTS CHANGEME COMPONENT ------ no tests have been written.', () => {

  const component = shallow(
      <Component />
  );

  expect( component.find( '.component_brace' ).text() ).toEqual( 'changeme' );

});
